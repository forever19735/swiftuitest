//
//  SwiftUITestApp.swift
//  SwiftUITest
//
//  Created by john.lin on 2020/12/17.
//

import SwiftUI

@main
struct SwiftUITestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
